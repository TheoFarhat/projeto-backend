const Express = require("express");
const router = Express();
const ClienteController = require("../controllers/ClienteController");
const ProdutoController = require("../controllers/ProdutoController");

//rotas para o cliente
router.post("/cliente", ClienteController.create);
router.get("/cliente/:id", ClienteController.show);
router.get("/cliente", ClienteController.index);
router.delete("/cliente/:id", ClienteController.destroy);
router.put('/cliente/:id', ClienteController.update);


//rotas para o produto
router.post("/produto", ProdutoController.create);
router.get("/produto/:id", ProdutoController.show);
router.get("/produto", ProdutoController.index);
router.delete("/produto/:id", ProdutoController.destroy);
router.put('/produto/:id', ProdutoController.update);

//rotas relacionamento cliente-produto
router.post("/produto/relationCliente/produto/:produtoId/cliente/:clienteId", ProdutoController.relationCliente);
router.delete("/produto/removeRelationCliente/produto/:produtoId/cliente/:clienteId", ProdutoController.removeRelationCliente);

module.exports = router;


