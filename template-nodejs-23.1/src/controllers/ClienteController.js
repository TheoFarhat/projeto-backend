const Cliente = require("../models/Cliente")

async function create (req, res) {
    try {
        const cliente = await Cliente.create(req.body);
        return res.status(201).json({message: "Cliente cadastrado com sucesso!", Cliente: cliente})

    }
    catch(err) {
        return res.status(500).json(err);
    }
}

async function show (req,res) {
    const {id} = req.params;
    try {
        const cliente = await Cliente.findByPk(id);
        if(cliente) {
            return res.status(200).json({message: " Informações do cliente "+ id, cliente: cliente});
        }
        throw new Error ();
    }
    catch(err) {
        return res.status(500).json("Cliente não encontrado.");

    }
}


async function index (req, res) {
    try {
        const clientes = await Cliente.findAll();
        return res.status(200).json({message: "Todos os clientes listados.", clientes: clientes});
    }
    catch(err) {
        return res.status(500).json(err);
    }
}

async function destroy (req,res) {
    const {id} = req.params;
    try {
        const deleted = await Cliente.destroy({where: {id:id}});
        if(deleted) {
            return res.status(200).json("Cliente deletado com sucesso!");

        }
        throw new Error ();
    }
    catch(err) {
        return res.status(500).json("Cliente não encontrado.");
    }
}


async function update (req,res) {
    const {id} = req.params;
    try {
        const [updated] = await Cliente.update(req.body, {where: {id: id}});
        if(updated) {
            const cliente = await Cliente.findByPk(id);
            return res.status(200).json({message: "Cliente atualizado!", cliente});

        }
        throw new Error();
    } catch(err) {
        return res.status(500).json("Cliente não encontrado.");
    }
}



module.exports = {
    create, 
    show,
    index,
    destroy,
    update
}