
const Produto = require("../models/Produto")
const Cliente = require("../models/Cliente")

async function create (req, res) {
    try {
        const produto = await Produto.create(req.body);
        return res.status(201).json({message: "Produto cadastrado com sucesso!", Produto: produto})

    }
    catch(error) {
        return res.status(500).json(error);
    }
}


async function show (req,res) {
    const {id} = req.params;
    try {
        const produto = await Produto.findByPk(id);
        if(produto) {
            return res.status(200).json({message: " Informações do produto "+ id, produto: produto});
        }
        throw new Error ();
    }
    catch(err) {
        return res.status(500).json("Produto não encontrado.");

    }
}


async function index (req, res) {
    try {
        const produtos = await Produto.findAll();
        return res.status(200).json({message: "Todos os produtos listados.", produtos: produtos});
    }
    catch(err) {
        return res.status(500).json(err);
    }
}

async function destroy (req,res) {
    const {id} = req.params;
    try {
        const deleted = await Produto.destroy({where: {id:id}});
        if(deleted) {
            return res.status(200).json("Produto deletado com sucesso!");

        }
        throw new Error ();
    }
    catch(err) {
        return res.status(500).json("Produto não encontrado.");
    }
}


async function update (req,res) {
    const {id} = req.params;
    try {
        const [updated] = await Produto.update(req.body, {where: {id: id}});
        if(updated) {
            const produto = await Produto.findByPk(id);
            return res.status(200).json({message: "Produto atualizado!", produto});

        }
        throw new Error();
    } catch(err) {
        return res.status(500).json("Produto não encontrado.");
    }
}


async function relationCliente (req,res) {
    const {produtoId, clienteId} = req.params;
    try {
        const cliente = await Cliente.findByPk(clienteId);
        const produto = await Produto.findByPk(produtoId);
        await produto.setCliente(cliente);
        return res.status(200).json({message: `O cliente ${cliente.nome} possui o produto: `, produto});
    } catch (err) {
        return res.status(500).json("Produto ou cliente não encontrado.");
    }
}

async function removeRelationCliente (req,res) {
    const {produtoId, clienteId} = req.params;
    try {
        const cliente = await Cliente.findByPk(clienteId);
        const produto = await Produto.findByPk(produtoId);
        await produto.setCliente(null);
        return res.status(200).json({message: `O cliente ${cliente.nome} não possui mais o produto: `, produto});
    } catch (err) {
        return res.status(500).json("Produto ou cliente não encontrado.");
    }
}

module.exports = {
    create, 
    show,
    index,
    destroy,
    update,
    relationCliente,
    removeRelationCliente
}
