const DataTypes = require("sequelize")
const sequelize = require("../config/sequelize")

const Cliente = sequelize.define("Cliente", {

    nome: {
        type: DataTypes.STRING,
        allowNull: false
    },
    idade: {
        type: DataTypes.INTEGER,
        allowNull:false
    },
    numeroDeTelefone: {
        type: DataTypes.STRING,
        allowNull:false
    },
    cpf: {
        type: DataTypes.STRING,
        allowNull:false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    }
}
)


Cliente.associate = (models) => {
    Cliente.hasMany(models.Produto)
}

module.exports = Cliente;