const DataTypes = require("sequelize")
const sequelize = require("../config/sequelize")

const Produto = sequelize.define("Produto", {

    nome: {
        type: DataTypes.STRING,
        allowNull: false
    },
    categoria: {
        type: DataTypes.STRING,
        allowNull:false
    },
    estado: {
        type: DataTypes.STRING,
        allowNull:false
    },
    marca: {
        type: DataTypes.STRING,
        allowNull:false
    },
    preco: {
        type: DataTypes.DOUBLE,
        allowNull: false
    }
}
)

Produto.associate = (models) => {
    Produto.belongsTo(models.Cliente)
}



module.exports = Produto;